/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package namacomedash;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.scene.shape.Circle;
import nicolive.comment.Comment;
import nicolive.comment.NiconamaCommentManager;
import test.GetSystemIn;

/**
 *
 * @author akigon
 */
public class Game {
    public static boolean IS_GAMEOVER;
    public static boolean IS_TIMER;
    private static final int[][] STAGE = new GameMap().getGameMap();
    private int hight = 1;
    private int width = 1;
    private static String email;
    private static String pass;
        
    public void start(String id) {
        
        ExecutorService service = Executors.newSingleThreadExecutor();
        Runnable task = () -> {
            System.out.println("Game thread --> start");
            new MoveProcess();
            
            //本番用ニココメ入力モード
            new NiconamaCommentManager(email, pass, id) {
                
                @Override
                public void onChat(Comment cmnt) {
                    System.out.println("onChat-->" + cmnt.getComment());
                    if (!IS_GAMEOVER) {
                        movementJugde(cmnt.getComment());
                    }
                }
            };
            
            //開発用キーボード入力モード
//            new GetSystemIn() {
//
//                @Override
//                public void onSystemIn(String in) {
//                    System.out.println("onSystemIn -->" + in);
//                    if (!IS_GAMEOVER) {
//                        movementJugde(in);
//                    }
//                }
//            }.doGet();

            
        };
        service.submit(task);
    }
    
    
    private int movementJugde(String s) {
        int h = hight;
        int w = width;
        if(s.equals("右")||s.equals("みぎ")) 
            w++;
        else if(s.equals("左")||s.equals("ひだり"))
            w--;
        else if(s.equals("下")||s.equals("した"))
            h++;
        else if(s.equals("上")||s.equals("うえ"))
            h--;
        else
            return 0;
        System.out.println("h="+h+" w="+w);
        int element;
        if((element = STAGE[h][w]) > 0) {
            new MoveProcess().locationChanged(hight, width, h, w, element);
             
            hight = h;
            width = w;
        }
        
        return element;
    }
    
    public void resetGame() {
        String deleteObject = "obj"+String.valueOf(hight)+"_"+String.valueOf(width);
        Circle deleteObj = (Circle)GameDocumentController.moveObject.get(deleteObject);
        Circle defaultObj = (Circle)GameDocumentController.moveObject.get("obj1_1");
        hight = 1;
        width = 1;
        Platform.runLater(() -> {
            deleteObj.setVisible(false);
            defaultObj.setVisible(true);
        });
        
    }
    
    public static void setEmail(String email) {
        Game.email = email;
    }
    
    public static void setPass(String pass) {
        Game.pass = pass;
    }
    
    public static String getEmail() {
        return Game.email;
    }
    
    public static String getPass() {
        return Game.pass;
    }
    
}
