/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package namacomedash;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import namacomedash.NamaComeDash;
import namacomedash.Game;

/**
 * FXML Controller class
 *
 * @author akigon
 */
public class LoginConfDocumentController implements Initializable {
    @FXML
    private TextField loginConf_email;
    @FXML
    private TextField loginConf_password;
    @FXML
    private Button loginConf_commitButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void onLoginConf_commit(MouseEvent event) throws Exception {
        Game.setEmail(loginConf_email.getText());
        Game.setPass(loginConf_password.getText());
        NamaComeDash.getInstance().showGameDocment();
    }
    
}
