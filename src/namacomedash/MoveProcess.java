/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package namacomedash;

import namacomedash.GameTimer;
import namacomedash.GameDocumentController;
import namacomedash.Game;
import javafx.application.Platform;
import javafx.scene.shape.Circle;

/**
 *
 * @author akigon
 */
public class MoveProcess {
    
       
    public void locationChanged(int y, int x, int newY, int newX, int element) {
        GameTimer.getInstance().start();
        
        String oldObjectYX = "obj" + String.valueOf(y) + "_" + String.valueOf(x);
        String newObjectYX = "obj" + String.valueOf(newY) + "_" + String.valueOf(newX);
        Circle oldObj = (Circle) GameDocumentController.moveObject.get(oldObjectYX);
        Circle newObj = (Circle) GameDocumentController.moveObject.get(newObjectYX);
        
        Platform.runLater(() -> {
            oldObj.setVisible(false);
            newObj.setVisible(true);
        });
        
        if (element == 1) {
            GameTimer.getInstance().stop();
            Game.IS_GAMEOVER = true;
            Platform.runLater(() -> {
                GameDocumentController.TEXT_GAME.setVisible(true);
                GameDocumentController.TEXT_OVER.setVisible(true);
            });
            return;
        }
        if (newObjectYX.equals(GameDocumentController.FINISH_OBJECT_NAME)) {
            GameTimer.getInstance().stop();
            Game.IS_GAMEOVER = true;
            Platform.runLater(() -> {
                GameDocumentController.TEXT_FINISH.setVisible(true);
            });
           
        }
    }

    
    
}
