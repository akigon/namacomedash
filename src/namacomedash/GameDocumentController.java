/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package namacomedash;


import namacomedash.Game;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import namacomedash.NamaComeDash;

/**
 *
 * @author akigon
 */

public class GameDocumentController implements Initializable, OnGameTimerListener {
    public static final String FINISH_OBJECT_NAME = "obj4_3";
    public static Map moveObject;
    public static Text TEXT_FINISH;
    public static Text TEXT_GAME;
    public static Text TEXT_OVER;
    
    @FXML
    private TextField lv_id;
    @FXML
    private Text text_finish;
    @FXML
    private Text timer;
    @FXML
    private Text text_game;
    @FXML
    private Text text_over;
    @FXML
    private Circle obj1_1;
    @FXML
    private Circle obj1_2;
    @FXML
    private Circle obj1_3;
    @FXML
    private Circle obj2_1;
    @FXML
    private Circle obj2_2;
    @FXML
    private Circle obj2_3;
    @FXML
    private Circle obj3_1;
    @FXML
    private Circle obj3_2;
    @FXML
    private Circle obj3_3;
    @FXML
    private Circle obj4_2;
    @FXML
    private Circle obj4_3;
     
        
    private Game game;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        game = new Game();
        
        GameTimer.getInstance().setOnGameTimerListener(this);
        moveObject = createObjectMap();
        TEXT_FINISH = text_finish;
        TEXT_GAME = text_game;
        TEXT_OVER = text_over;
    }
    
    @FXML
    private void onRetryGameButton() {
        game.resetGame();
        Game.IS_GAMEOVER = false;
        TEXT_FINISH.setVisible(false);
        TEXT_GAME.setVisible(false);
        TEXT_OVER.setVisible(false);
        timer.setText("0");
    }
    
    @FXML
    private void onEndGameButton() {
        Platform.exit();
    }
    
    @FXML
    private void onConnectButton() {
        String id = lv_id.getText();
        if(!id.isEmpty()) {
            System.out.println("pushConnect-->" + id);
            game.start(id);
        }
    }
           
    private Map createObjectMap() {
        Map map = new HashMap();
        
        map.put("obj1_1", obj1_1);
        map.put("obj1_2", obj1_2);
        map.put("obj1_3", obj1_3);
        map.put("obj2_1", obj2_1);
        map.put("obj2_2", obj2_2);
        map.put("obj2_3", obj2_3);
        map.put("obj3_1", obj3_1);
        map.put("obj3_2", obj3_2);
        map.put("obj3_3", obj3_3);
        map.put("obj4_2", obj4_2);
        map.put("obj4_3", obj4_3);
        
        return map;
    }

    @Override
    public void onTimerCountChanged(String count) {
        Platform.runLater(() -> {
            timer.setText(count);
        });
        
    }
    
    
    //ログイン設定押下処理
    @FXML
    private void onLoginConf() throws Exception {
        NamaComeDash.getInstance().showLoginConfDocument();
    }
     
    
    

   
    
    
    
}
