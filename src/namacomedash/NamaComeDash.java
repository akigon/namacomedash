/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package namacomedash;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author akigon
 */
public class NamaComeDash extends Application {
    public static NamaComeDash singleton;
    private Stage stage;
    
    public static void main(String[] args) throws IOException {
        launch(args);
                         
    }
        
    @Override
    public void start(Stage stage) throws Exception {
        singleton = this;
        this.stage = stage;
        showGameDocment();
    
    }
    
    public void showGameDocment() throws Exception {
        showPage(stage, "GameDocument.fxml");
    }
    
    public void showLoginConfDocument() throws Exception {
         showPage(stage, "LoginConfDocument.fxml");
    }
    
    @Override
    public void stop() {
        System.exit(0);
        
    }
        
    public void showPage(Stage stage, String res) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource(res));
        stage.setScene(new Scene(parent));
        stage.show();
    }
    
    public static NamaComeDash getInstance() {
        return singleton;
    }

     
}
