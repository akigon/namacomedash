/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package namacomedash;

import namacomedash.Game;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.Timer;


/**
 *
 * @author akigon
 */
public class GameTimer implements ActionListener {
    private static GameTimer instance = new GameTimer();
    private OnGameTimerListener listener;
    private ExecutorService service;
    private Timer timer;
    private int count;
    
    private GameTimer() {
        service = Executors.newSingleThreadExecutor();
        timer = new Timer(100, this);
    }
    
    public static GameTimer getInstance() {
        return instance;
    }
    
    public void start() {
        if(!Game.IS_TIMER) {
            Runnable task = () -> {
                count = 0;
                timer.start();
            };
            service.submit(task);
            Game.IS_TIMER = true;
        }
    }
    
    public void stop() {
        Game.IS_TIMER = false;
        timer.stop();
        if(service.isShutdown()) System.out.println("timerTask-->Shutdown!");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        count++;
        if(listener != null) listener.onTimerCountChanged(String.valueOf(count));
    }
    
    public void setOnGameTimerListener(OnGameTimerListener listener) {
        this.listener = listener;
    }
    
}
