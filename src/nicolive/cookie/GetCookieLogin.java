package nicolive.cookie;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class GetCookieLogin {

	public GetCookieLogin() {
            // TODO 自動生成されたコンストラクター・スタブ
	}

	/**
	 * ニコニコにログインしてCookieを取得する。
         * @param mail
         * @param pw
	 * @return Cookie文字列を返す。
	 */
	public String nicoLiveLogin(String mail, String pw) {
            Writer writer = null;
            try {
                URL url = new URL("https://secure.nicovideo.jp/secure/login?site=niconico");
                HttpsURLConnection con =  (HttpsURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.connect();

                //POSTデータ
                writer = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
                writer.write("mail=" + URLEncoder.encode(mail, "UTF-8"));
                writer.write("&password=" + URLEncoder.encode(pw, "UTF-8"));
                writer.write("&next_url=");
                writer.flush();

                int response = con.getResponseCode();
                System.out.println("Response: " + response);
            
                /*
                 * レスポンスヘッダ確認表示用
                 */
//              Map headers = con.getHeaderFields();
//              Iterator it = headers.keySet().iterator();
//              System.out.println("レスポンスヘッダ:");
//              while (it.hasNext()){
//                  String key= (String)it.next();
//                  System.out.println("  " + key + ": " + headers.get(key));
//              }

                return getUser_session4Hedder(con);

            } catch (IOException e) {
		e.printStackTrace();
            } finally {
		try {
                    writer.close();
		} catch (IOException e) {
                    e.printStackTrace();
		}
            }
            return null;
	}

	 
	/**
	 * Httpヘッダからuser_sessionを取得する。
	 * @param HttpURLConection
	 * @return user_session
	 */
	private String getUser_session4Hedder(HttpsURLConnection con) {
            Map headers = con.getHeaderFields();
            Iterator it = headers.keySet().iterator();
            System.out.println("レスポンスヘッダ:");
            while (it.hasNext()){
                String key= String.valueOf(it.next());
                System.out.println(key);
                if(key.equals("Set-Cookie")) {
                    String cookie = String.valueOf(headers.get(key));
                    cookie = cookie.substring(1, cookie.length()-1);
                    System.out.println("HeadersGetCookieTags--> "+cookie);
                    String[] array = cookie.split(";");

                    for (String s : array) {
                        String[] div1 = s.split(",");
                        for (String div2 : div1) {
                            String[] user_cookie = div2.split("=");
//                          System.out.println(user_cookie[0].trim());
                            if (user_cookie[0].trim().equals("user_session") && !user_cookie[1].equals("deleted")) {
//                              System.out.println(user_cookie[1]);
                                return user_cookie[1].trim();
                            }
                        }
                    }
                }
            }
            return null;
	}


}
