package nicolive.comment;

public class Comment {
	private String no;
	private String vpos;
	private String date;
	private String date_usec;
	private String mail;
	private String user_id;
	private String premium;
	private String anonymity;
	private String locale;
	private String comment;

	public Comment(String no, String vpos, String date, String date_usec, String mail, String user_id, String premium,
			String anonymity, String locale, String comment) {
		this.no = no;
		this.vpos = vpos;
		this.date = date;
		this.date_usec = date_usec;
		this.mail = mail;
		this.user_id = user_id;
		this.premium = premium;
		this.anonymity = anonymity;
		this.locale = locale;
		this.comment = comment;
	}

	public String getNo() {
		return no;
	}

	public String getVpos() {
		return vpos;
	}

	public String getDade() {
		return date;
	}

	public String getDate_usec() {
		return date_usec;
	}

	public String getMail() {
		return mail;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getPremium() {
		if(premium == null)
			return "0";
		return premium;
	}

	public String getAnonymity() {
		if(anonymity == null)
			return "0";
		return anonymity;
	}

	public String getLocale() {
		return locale;
	}

	public String getComment() {
		return comment;
	}

}
