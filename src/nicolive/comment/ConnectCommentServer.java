package nicolive.comment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

public class ConnectCommentServer {
	Map<String, String> values;
	BufferedReader br;


	public ConnectCommentServer(Map<String, String> values) {
		this.values = values;
                System.out.println("commentServerADRR-->"+values.get("addr"));
                System.out.println("commentServerPORT-->"+values.get("port"));
	}

	public void doConnect() {

//		System.out.println(values.get("addr")+values.get("port"));

			Socket socket;
			try {
				socket = new Socket(values.get("addr"), Integer.parseInt(values.get("port")));

			if(socket.isConnected()) {
				System.out.println(socket.getRemoteSocketAddress() + " : " + "connected!");

				OutputStream out = socket.getOutputStream();
				InputStream in = socket.getInputStream();

				PrintWriter pw = new PrintWriter(new OutputStreamWriter(out));
				pw.println("<thread thread=\"" + values.get("thread") + "\" version=\"20061206\" res_from=\"-0\" scores=\"1\" />\0");
				pw.flush();

				br = new BufferedReader(new InputStreamReader(in));
				int c;
				while ((c = br.read()) != -1) {

					if ((char) c == 0)
						break;

				}
				System.out.println("SUCSSES:Comments in standby...");

			}
			} catch (NumberFormatException | IOException e) {
				e.printStackTrace();
			}

	}


	public String getChat() {

		try {
			StringBuilder sb = new StringBuilder();
			int c;
			while ((c = br.read()) != -1) {
				if ((char) c != 0) {
					sb.append((char) c);
					if (((char) c == '>') && (sb.toString().endsWith("</chat>")))
						return sb.toString();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "";

	}


}
