package nicolive.comment;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;



import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GetCommentServerTicket {

	public GetCommentServerTicket() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	/**
	 * Cookieと放送IDを元にコメントサーバーのアドレス、Port、スレッドIDを取得
         * @param lvID
         * @param cookie
         * @return 
	 */
	public Map<String, String> getTicket(String lvID, String cookie){
            InputStream is = null;
               
            try {
                URL url = new URL("http://live.nicovideo.jp/api/getplayerstatus?v="+lvID);
                URLConnection con = url.openConnection();
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                con.setRequestProperty("Accept-Language", "ja;q=0.7,en;q=0.3");
                con.setRequestProperty( "Cookie" , "user_session=" + cookie );
                con.connect();

                //コメントサーバにアクセスするための情報をXMLで取得する。
                is = con.getInputStream();
                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);

                //XMLから必要な情報を取り出す。
                Map<String, String> ms = new HashMap<>();
                Element root = doc.getDocumentElement();
                System.out.println("RootTagName：" + root.getTagName() + "  Status:" + root.getAttribute("status"));
                NodeList nodeList = root.getElementsByTagName("ms");

                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node child = nodeList.item(i);
                    if(child.getNodeName().equals("ms")) {
                        NodeList array = child.getChildNodes();
                        for(int j=0; j<array.getLength(); j++) {
                            Node ch = array.item(j);

                            if(ch.getNodeName().equals("addr"))
                                    ms.put("addr",  ch.getTextContent());

                            if(ch.getNodeName().equals("port"))
                                    ms.put("port",  ch.getTextContent());

                            if(ch.getNodeName().equals("thread"))
                                    ms.put("thread",  ch.getTextContent());
                        }
                    }
                }
                return ms;
            } catch(IOException | ParserConfigurationException | SAXException | DOMException e) {

            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    
                }
            }
            return null;
	}

}
