package nicolive.comment;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import nicolive.cookie.GetNicoCookie;

/**
 * ニコ生のコメントを取得する。
 * @author akigon
 * ver.1.0
 *
 */

public abstract class NiconamaCommentManager {
	
	public NiconamaCommentManager() {
		
	}

	public NiconamaCommentManager(final String mail, final String pw, final String lvID) {

		String cookie = new GetNicoCookie().GetString(mail, pw);
		System.out.println("user_session-->"+cookie);
		final Map<String, String> value = new GetCommentServerTicket().getTicket(lvID, cookie);

		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				
				
				ConnectCommentServer connect = new ConnectCommentServer(value);
				connect.doConnect();

				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                	        DocumentBuilder builder;

				while(true) {

					try {
						builder = factory.newDocumentBuilder();
						Node root = builder.parse(new ByteArrayInputStream(connect.getChat().getBytes()));
						Node child = root.getFirstChild();
						String no = child.getAttributes().getNamedItem("no").getNodeValue();
//						String vpos = child.getAttributes().getNamedItem("vpos").getNodeValue();
						String date = child.getAttributes().getNamedItem("date").getNodeValue();
//						Node date_usec = child.getAttributes().getNamedItem("date_usec");
//						Node mail = child.getAttributes().getNamedItem("mail");
						String user_id = child.getAttributes().getNamedItem("user_id").getNodeValue();
						String premium = null;
						if(child.getAttributes().getNamedItem("premium") != null)
							premium = child.getAttributes().getNamedItem("premium").getNodeValue();
						String anonymity = null;
						if(child.getAttributes().getNamedItem("anonymity") != null)
							anonymity = child.getAttributes().getNamedItem("anonymity").getNodeValue();
//						String locale = child.getAttributes().getNamedItem("locale").getNodeValue();

						Comment comment = new Comment(
								no,
								null,
								date,
								null,
								null,
								user_id,
								premium,
								anonymity,
								null,
								child.getTextContent());

						onChat(comment);

					} catch (SAXException | ParserConfigurationException | IOException e) {
						e.printStackTrace();
					}

				}
			}
		});
		th.start();

	}

	public abstract void onChat(Comment comment);







}
