/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.EventListener;

/**
 *
 * @author akigon
 */
public interface OnSystemIn extends EventListener{
    public void onSystemIn(String in);
}
