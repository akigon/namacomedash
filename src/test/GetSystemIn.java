/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akigon
 */
public abstract class GetSystemIn {
    private OnSystemIn listener;
    
    
          
    public void doGet() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String str;
            while(true) {
                 str = br.readLine();
                 if(listener != null) {
                     listener.onSystemIn(str);
                 } else {
                     onSystemIn(str);
                 }
                 System.out.println(str);
            }
        } catch (IOException ex) {
                    
        }
    }
    
    public void setOnSystemIn(OnSystemIn listener) {
        this.listener = listener;
    }
    
    public abstract void onSystemIn(String in);
    
    
    
    
}
